# Qubole

## Question 1

1) Find the top K domains for an interval (start, end) from a url log file containing lots of records of the form (url, userid, timestamp). 

You can assume that the data is present in CSV or JSON files or is present in an rdbms.



## Question 2

2) A company wanted to find the top month in terms of sales every year for last N years when they did max revenue for that year. Output should be sorted in descending order of monthly revenue. 

Output format:

Month     Year     Revenue
-----------------------------------------
04.     2011.         10m
06.     2012.           9m

You can assume that the data contains (order details, date, order_price) in CSV or JSON files or is present in an rdbms.



## Instructions

A) You can choose to code in Scala or Python or SQL. But, you should use spark APIs and constructs to do the same.
B) You can use any Spark implemention for this.
C) Please ensure that the code compiles and executes. We're not looking just for pseudo code or the design in this case.
D) You have to attempt both questions.


## Solutions

I have created 2 solutions based in command line arguments. For a larger number of arguments properties / config files are advised.

I haven't put much emphasis on the test because I generated the dataset myself. In a real world scenario I would create small data sets to test all the corner cases and validate the operations against it. In this case the tests are just execute the jobs locally based on the fake datasets.   