package com.joseestudillo.qubole

import org.apache.spark.sql.SparkSession

/**
  *
  * Spark job to solve question 2
  *
  * Assumptions:
  *  - all the dates are in UTC, so there is no need to consider timezones.
  *  - the csv file doesn't contain headers
  *  - for more complicated operations with dates or different formats I would consider the conversion to timestamp
  *
  * @author Jose Estudillo
  */
object Question1 {

  case class Params(inputFilePath: String, start: String, end: String, k: Int)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("BasicDriver")
      .enableHiveSupport()
      .getOrCreate()

    if (args.length >= 4) {
      // assumes proper validation
      val config = Params(args(0),args(1),args(2), args(3).toInt)
      run(spark, config)
    } else {
      println("Usage: csv file path required")
      System.exit(1)
    }
  }

  def run(spark: SparkSession, params: Params): Unit = {
    val colNames = Seq("url", "userid", "timestamp")
    val df = spark.read.option("header", "false").csv(params.inputFilePath)
      .toDF(colNames: _*)
    df.show()

    //with DataFrame API
    import org.apache.spark.sql.functions._
    val resultDf = df
      //filter to have only the selected time range
      .filter(df("timestamp").between(params.start, params.end))
      //group by URL, the count will be called n_hits
      .groupBy(df("url")).agg(count(df("*")).as("n_hits"))
      //order by the number of hits descending
      .orderBy(desc("n_hits"))
      // get the top k
      .limit(params.k)
    resultDf.show()

    //with Query
    df.createOrReplaceTempView("tmp_table")
    val sql = s"""
         |SELECT url, count(*) as n_hits FROM tmp_table
         |WHERE '${params.start}' <= timestamp and timestamp <= '${params.end}'
         |GROUP BY url
         |ORDER BY count(*) DESC
         |LIMIT ${params.k}""".stripMargin

    val resultSQL = df.sqlContext.sql(sql)
    resultSQL.show()

    // the exercise doesnt say anything about what to do with the output so I'm just showing it.

  }

}
