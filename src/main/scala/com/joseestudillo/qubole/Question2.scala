package com.joseestudillo.qubole

import com.joseestudillo.qubole.Question1.Params
import org.apache.spark.sql.{Row, SparkSession}

/**
  *
  * Spark job to solve question 2
  *
  * Assumptions:
  *  - all the dates are in UTC, so there is no need to consider timezones.
  *  - the csv file doesn't contain headers
  *  - for more complicated operations with dates or different formats I would consider the conversion to timestamp
  *
  * @author Jose Estudillo
  */
object Question2 {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("BasicDriver")
      .enableHiveSupport()
      .getOrCreate()

    if (args.length > 0) {
      val inputFilePath = args(0)
      run(spark, inputFilePath)
    } else {
      println("Usage: csv file path required")
      System.exit(1)
    }
  }

  def run(spark: SparkSession, inputFilePath: String): Unit = {
    val colNames = Seq("details", "date", "order_price")
    val df = spark.read.option("header", "false").csv(inputFilePath).toDF(colNames: _*)

    val dfYearMonth = df
      .select(df("date").substr(6, 2).as("month"), df("date").substr(1, 4).as("year"), df("order_price"))

    // with DataFrame / RDDs
    import org.apache.spark.sql.functions.sum
    import spark.implicits._
    val dfMonthlyRevenue = dfYearMonth.groupBy($"month", $"year").agg(sum("order_price").as("revenue"))

    spark.createDataFrame(
      dfMonthlyRevenue.rdd
        // group by year
        .groupBy { case Row(_, year, _) => year }
        // each group (key, rows) will generate one row with the maximum revenue
        .map { case (_, rows) => rows.maxBy(r => r.getDouble(r.fieldIndex("revenue"))) },
      dfMonthlyRevenue.schema).show()

    // with SQL
    dfMonthlyRevenue.createOrReplaceTempView("tmp_table")
    val sql =
      s"""
         |SELECT mt.month, mt.year, mt.revenue
         |FROM tmp_table AS mt
         |WHERE mt.revenue ==
         |  (SELECT MAX(st.revenue)
         |   FROM tmp_table AS st
         |   WHERE st.year = mt.year)
           """.stripMargin

    val resultSQL = dfMonthlyRevenue.sqlContext.sql(sql)
    resultSQL.show()
  }

  def run2(spark: SparkSession, params: Params): Unit = {
    val colNames = Seq("url", "userid", "timestamp")
    val df = spark.read.option("header", "false").csv(params.inputFilePath)
      .toDF(colNames: _*)
    df.show()

    //with DataFrame API
    import org.apache.spark.sql.functions._
    val resultDf = df
      //filter to have only the selected time range
      .filter(df("timestamp").>=(params.start).&&(df("timestamp").<=(params.end)))
      //group by URL, the count will be called n_hits
      .groupBy(df("url")).agg(count(df("*")).as("n_hits"))
      //order by the number of hits descending
      .orderBy(desc("n_hits"))
      // get the top k
      .limit(params.k)
    resultDf.show()

    //with Query
    df.createOrReplaceTempView("tmp_table")
    val sql =
      s"""
         |SELECT url, count(*) as n_hits FROM tmp_table
         |WHERE '${params.start}' <= timestamp and timestamp <= '${params.end}'
         |GROUP BY url
         |ORDER BY count(*) DESC
         |LIMIT ${params.k}""".stripMargin

    val resultSQL = df.sqlContext.sql(sql)
    resultSQL.show()

    // the exercise doesnt say anything about what to do with the output so I'm just showing it.
  }
}
