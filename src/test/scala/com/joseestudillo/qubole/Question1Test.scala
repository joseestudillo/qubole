package com.joseestudillo.qubole

/**
  *
  * Test to run the spark job for the question 1
  *
  * @author Jose Estudillo
  */
class Question1Test extends SparkTest {

  test("question 1 test") {
    val params = Question1.Params(
      SparkUtils.getClasspathFilePath("q1.csv"),
      "2017-01-01 00:00",
      "2017-03-01 23:59",
      3
    )
    Question1.run(spark, params)
    // notice that for any test, the data must be collected, as the test operations are not serializable
  }

}
