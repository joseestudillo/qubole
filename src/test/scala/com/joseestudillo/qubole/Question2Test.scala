package com.joseestudillo.qubole

/**
  *
  * Test to run the spark job for the question 2
  *
  * @author Jose Estudillo
  */
class Question2Test extends SparkTest {

  test("question 2 test") {
    val inputPath: String = SparkUtils.getClasspathFilePath("q2.csv")
    Question2.run(spark, inputPath)
    // notice that for any test, the data must be collected, as the test operations are not serializable
  }
}
