package com.joseestudillo.qubole

/**
  *
  * Created on 10/09/2017.
  *
  * @author Jose Estudillo
  */
object SparkUtils {

  /**
    * Returns the full path for a file in the classpath so it can be used with a local spark context
    *
    * @param classpathRelativePath relative path to a file in the classpath
    *
    * @return full path for a file in the classpath
    */
  def getClasspathFilePath(classpathRelativePath: String): String = {
    Thread.currentThread.getContextClassLoader.getResource(classpathRelativePath).getFile
      //check for windows paths
      .replaceAll("^\\s*.:", "").replaceAll("\\\\", "/")
  }
}
