package com.joseestudillo.qubole

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfterAll, FunSuite}

/**
  * Base class to handle the spark session to test spark jobs
  *
  * @author Jose Estudillo
  */
class SparkTest extends FunSuite with BeforeAndAfterAll {

  lazy val spark: SparkSession = this.session
  private var session: SparkSession = _

  override def beforeAll() {
    val conf = new SparkConf()
      .setAppName(getClass.getName)
      .setMaster("local[*]")
      .set("spark.driver.allowMultipleContexts", "true")
      .set("spark.sql.warehouse.dir", s"/tmp/${classOf[SparkTest].getPackage.getName}/spark-warehouse")
    session = SparkSession.builder()
      .appName("TestDriver")
      .config(conf)
      .enableHiveSupport() //it may fail if the required jars are not present.
      .getOrCreate()
  }

  override def afterAll() {
    spark.stop()
  }
}
